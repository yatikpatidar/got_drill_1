// - 2. Write a function called `peopleByHouses` which counts the total number of people in different houses in the `got` variable defined in `data.js` file.

function peopleByHouses(got){

    try{
        if(typeof(got) != 'object'){
            throw new Error("typeof of input data is not in object")
        }

        const peopleCounts = got['houses'].reduce((result , currKey)=>{

            result[currKey['name']] = currKey['people'].length

            return result

        },{})

        // console.log(peopleCounts)
        return Object.fromEntries(Object.entries(peopleCounts).sort())
    }
    catch(err){
        console.log(err.message)
    }

}
module.exports = peopleByHouses