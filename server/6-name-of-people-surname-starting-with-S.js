//- 6. Write a function called `surnameWithS` which returns a array of names of all the people in `got` variable whoes surname is starting with `S`(capital s).

function surnameWithS(got) {

    try {
        if (typeof (got) != 'object') {
            throw new Error("typeof of input data is not in object")
        }

        const nameList = got['houses'].reduce((list, currKey) => {

            if(currKey['name'][0] === 'S'){

                currKey['people'].reduce((innerList , innerKey)=>{
                    innerList.push(innerKey['name'])
                    return innerList
                } , list)
            }
            return list
        }, [])

        return nameList
    }
    catch (err) {
        console.log(err.message)
    }

}
module.exports = surnameWithS
