// - 1. Write a function called `countAllPeople` which counts the total number of people in `got` variable defined in `data.js` file.

function countAllPeople(got) {

    try {
        if (typeof (got) != 'object') {
            throw new Error("typeof of input data is not in object")

        }
        const peopleCounts = got['houses'].reduce((count, currKey) => {
            count += currKey['people'].length
            return count
        }, 0)

        return peopleCounts

    }
    catch (err) {
        console.log(err.message)
    }
}
// countAllPeople()

module.exports = countAllPeople