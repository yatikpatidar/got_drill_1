//- 5. Write a function called `nameWithA` which returns a array of names of all the people in `got` variable whose name includes `a` or `A`.

function nameWithA(got) {

    try {
        if (typeof (got) != 'object') {
            throw new Error("typeof of input data is not in object")
        }

        const nameList = got['houses'].reduce((list, currKey) => {
            return currKey['people'].reduce((innerList, innerKey) => {
                if (innerKey['name'].includes('a') || innerKey['name'].includes('A')) {
                    innerList.push(innerKey['name'])

                }
                return innerList
            }, list)


        }, [])

        return nameList
    }
    catch (err) {
        console.log(err.message)
    }

}
module.exports = nameWithA
