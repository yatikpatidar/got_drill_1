//- 8. Write a function called `peopleNameOfAllHouses` which returns an object with the key of the name of house and value will be all the people in the house in an array.

function peopleNameOfAllHouses(got) {

    try {
        if (typeof (got) != 'object') {
            throw new Error("typeof of input data is not in object")
        }

        const nameList = got['houses'].reduce((list, currKey) => {

            const houseNameList = currKey['people'].reduce((innerList , innerKey)=>{

                innerList.push(innerKey['name'])
                return innerList

            } ,[])

            list[currKey['name']] = houseNameList

            return list
            
        }, {})

        return Object.fromEntries(Object.entries(nameList).sort())
    }
    catch (err) {
        console.log(err.message)
    }

}
module.exports = peopleNameOfAllHouses
