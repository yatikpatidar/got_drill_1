//- 3. Write a function called `everyone` which returns a array of names of all the people in `got` variable.

function nameOfeveryone(got){

    try{
        if(typeof(got) != 'object'){
            throw new Error("typeof of input data is not in object")
        }

        const nameList = got['houses'].reduce((list , currKey)=>{
            
            currKey['people'].reduce((innerList , innerKey)=>{
                innerList.push(innerKey['name'])
                return innerList
            }, list)

            return list
        },[])
        
        return nameList
    }
    catch(err){
        console.log(err.message)
    }
    
}
module.exports = nameOfeveryone
